plugins {
    checkstyle
    alias(libs.plugins.fabric.loom)
}

version = project.property("mod_version")!!
group = project.property("maven_group")!!

repositories {
    maven("https://maven.parchmentmc.org")
}

dependencies {
    minecraft(libs.minecraft)
    mappings(loom.layered {
        officialMojangMappings()
        parchment("org.parchmentmc.data:parchment-${libs.versions.minecraft.get()}:${libs.versions.parchment.get()}@zip")
    })

    modImplementation(libs.fabric.loader)
}

tasks.processResources {
    val properties = mapOf(
        "version" to version,
        "java_version" to libs.versions.java.get(),
        "minecraft_version" to libs.versions.minecraft.get(),
        "loader_version" to libs.versions.fabric.loader.get(),
    )

    inputs.properties(properties)
    filesMatching(listOf("fabric.mod.json", "thehorrifyingmod.mixins.json")) {
        expand(properties)
    }
}

java {
    val javaVersion = JavaVersion.toVersion(libs.versions.java.get())
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
}
