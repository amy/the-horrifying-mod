package com.github.amyavi.thehorrifyingmod.mixin;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.commands.data.EntityDataAccessor;
import net.minecraft.world.entity.Entity;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

import java.util.UUID;

@Mixin(EntityDataAccessor.class)
public abstract class EntityDataAccessorMixin {
    @Final @Shadow
    private Entity entity;

    /**
     * @author amy
     * @reason I had to. Perhaps
     */
    @Overwrite
    public void setData(final CompoundTag other) {
        final UUID uUID = this.entity.getUUID();
        this.entity.load(other);
        this.entity.setUUID(uUID);
    }
}
