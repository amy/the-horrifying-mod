package com.github.amyavi.thehorrifyingmod.mixin;

import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.Dynamic2CommandExceptionType;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.network.chat.Component;
import net.minecraft.server.commands.RideCommand;
import net.minecraft.world.entity.Entity;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(RideCommand.class)
public abstract class RideCommandMixin {
    @Final
    @Shadow
    private static Dynamic2CommandExceptionType ERROR_MOUNT_FAILED;

    /**
     * @author amy
     * @reason I had to. Perhaps
     */
    @Overwrite
    private static int mount(final CommandSourceStack source, final Entity target, final Entity vehicle) throws CommandSyntaxException {
        if (!target.startRiding(vehicle, true)) {
            throw ERROR_MOUNT_FAILED.create(target.getDisplayName(), vehicle.getDisplayName());
        } else {
            source.sendSuccess(() ->
                    Component.translatable("commands.ride.mount.success", target.getDisplayName(), vehicle.getDisplayName()), true);
            return 1;
        }
    }
}
