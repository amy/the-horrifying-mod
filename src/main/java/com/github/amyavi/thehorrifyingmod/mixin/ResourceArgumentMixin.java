package com.github.amyavi.thehorrifyingmod.mixin;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import net.minecraft.commands.arguments.ResourceArgument;
import net.minecraft.world.entity.EntityType;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(ResourceArgument.class)
public class ResourceArgumentMixin {
    @WrapOperation(method = "getSummonableEntityType",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/world/entity/EntityType;canSummon()Z"))
    private static boolean getSpawnablePosIsInSpawnableBounds(final EntityType<?> instance, final Operation<Boolean> original) {
        return true;
    }
}
