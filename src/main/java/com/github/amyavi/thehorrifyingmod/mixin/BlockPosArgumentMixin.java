package com.github.amyavi.thehorrifyingmod.mixin;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import net.minecraft.commands.arguments.coordinates.BlockPosArgument;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(BlockPosArgument.class)
public class BlockPosArgumentMixin {
    @WrapOperation(method = "getLoadedBlockPos(Lcom/mojang/brigadier/context/CommandContext;Lnet/minecraft/server/level/ServerLevel;Ljava/lang/String;)Lnet/minecraft/core/BlockPos;",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/server/level/ServerLevel;hasChunkAt(Lnet/minecraft/core/BlockPos;)Z"))
    private static boolean getLoadedBlockPosHasChunkAt(final ServerLevel instance, final BlockPos blockPos, final Operation<Boolean> original) {
        return true;
    }

    @WrapOperation(method = "getLoadedBlockPos(Lcom/mojang/brigadier/context/CommandContext;Lnet/minecraft/server/level/ServerLevel;Ljava/lang/String;)Lnet/minecraft/core/BlockPos;",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/server/level/ServerLevel;isInWorldBounds(Lnet/minecraft/core/BlockPos;)Z"))
    private static boolean getLoadedBlockIsInWorldBounds(final ServerLevel instance, final BlockPos blockPos, final Operation<Boolean> original) {
        return true;
    }

    @WrapOperation(method = "getSpawnablePos",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/world/level/Level;isInSpawnableBounds(Lnet/minecraft/core/BlockPos;)Z"))
    private static boolean getSpawnablePosIsInSpawnableBounds(final BlockPos pos, final Operation<Boolean> original) {
        return true;
    }
}
